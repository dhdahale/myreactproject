import React,{useState} from 'react';
import {View,Text,StyleSheet,Button} from 'react-native';
import ColorCounter from '../components/ColorCounter';

  
const COLOR_INCREMENT = 15;


const SquareScreen = () => {
   const [Red,SetRed] = useState(0);
   const [Green,SetGreen] = useState(0);
   const [Blue,SetBlue] = useState(0);
   console.log(Red);
   
   const SetColor  = (color,change) => {
    // color === 'Red','Green','Blue'
    // change === +15,-15
    //if (color === "Red"){    (Initial Way to validate this)
    //    if (Red + change > 255 || Red + change < 0){
    //        return;
    //    }else{
    //        SetRed(Red + change);
    //    }
    //}
    // 2nd very nice way to validate this is using switch case and ternary operator
      switch (color){
          case "Red" :
            Red + change > 255 || Red + change < 0 ? null : SetRed(Red + change);
            return;
          case 'Green':
            Green + change > 255 || Green + change < 0 ? null : SetGreen(Green + change);
            return;
          case 'Blue':
            Blue + change > 255 || Blue + change < 0 ? null : SetBlue(Blue + change);
            return;
          default:
            return;
      }
  };

  return <View>
      <ColorCounter 
        onIncrease = {() => SetColor("Red" , COLOR_INCREMENT)} 
        onDecrease = {() => SetColor("Red" , -1 * COLOR_INCREMENT)}
        color='Red'
      />
      <ColorCounter 
        onIncrease = {() => SetColor('Green' , COLOR_INCREMENT)}
        onDecrease = {() => SetColor('Green' , -1 * COLOR_INCREMENT)}
        color = 'Green'
      />
      <ColorCounter 
        onIncrease = {() => SetColor('Blue' , COLOR_INCREMENT)}
        onDecrease = {() => SetColor('Blue' , -1 * COLOR_INCREMENT)}
        color = 'Blue'
      />
      <View style = {{height : 150 , width : 150 , backgroundColor : `rgb(${Red},${Green},${Blue})`}}/>
  </View>
};

const styles = StyleSheet.create({});

export default SquareScreen;