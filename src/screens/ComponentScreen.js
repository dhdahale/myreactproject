import React from 'react';
import {Text,StyleSheet,View} from 'react-native';

const ComponentScreen = () => {
  const name = 'Deva';

    return <View>
          <Text style={styles.textStyle}>Getting started with react native!</Text>
          <Text s={styles.substyleheader}>My name is {name}</Text>
         </View>
};

const styles = StyleSheet.create({
  textStyle: {
      fontSize: 45
  },
  substyleheader:{
    fontSize: 20
  }
});

export default ComponentScreen;