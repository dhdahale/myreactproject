import React, {useState} from 'react';
import {View,Text,StyleSheet,Button} from 'react-native';

const CounterScreen = () => {
    const [counter,Setcounter] = useState(0)
  return <View>
      <Button 
       title = 'Increase'
       onPress={() => {
           Setcounter(counter+1)
           //console.log(counter)
       }}
      />
      <Button 
        title = 'Decrease'
        onPress={() => {
            Setcounter(counter-1)
            //console.log(counter)
        }}
      />
      <Text>Current count:{counter}</Text>
  </View>
};

const styles = StyleSheet.create({});

export default CounterScreen;