import React from 'react';
import { Text, View,StyleSheet, FlatList} from 'react-native';

const ListScreen = () => {
    const friends = [
        {name : 'Friend #1', Age : 20},
        {name : 'Friend #2', Age : 45},
        {name : 'Friend #3', Age : 55},
        {name : 'Friend #4', Age : 67},
        {name : 'Friend #5', Age : 75},
        {name : 'Friend #6', Age : 85},
        {name : 'Friend #7', Age : 35},
        {name : 'Friend #8', Age : 53},
        {name : 'Friend #9', Age : 65}
    ];
  return <FlatList
           
           keyExtractor = {(friend) => friend.name}
           data = {friends}
           renderItem = {({item}) => {
                return <Text style={styles.textStyle}>{item.name} - Age {item.Age}</Text>;
           }}
         />
};

const styles = StyleSheet.create({
    textStyle: {
        marginVertical: 10
    }
});

export default ListScreen;