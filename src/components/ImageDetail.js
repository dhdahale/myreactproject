import React from 'react';
import {View,Text,StyleSheet,Image} from 'react-native';

const ImageDetail = ({imagesource,title,imagescore}) => {
    
  return <View> 
     <Image source = {imagesource} />
     <Text>{title}</Text>
     <Text>Image Score - {imagescore}</Text>
  </View>
};

const styles = StyleSheet.create({});

export default ImageDetail;